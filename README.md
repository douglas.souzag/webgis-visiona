[![Nodejs](https://img.shields.io/badge/nodejs-v12.18.1-<COLOR>.svg)](https://shields.io/)
[![Axios](https://img.shields.io/badge/axios-v0.19.2-red.svg)](https://shields.io/)
[![Mapbox](https://img.shields.io/badge/mapbox-v1.10.1-green.svg)](https://shields.io/)
[![Vue](https://img.shields.io/badge/vue-v2.6.11-blue.svg)](https://shields.io/)
[![Vuetify](https://img.shields.io/badge/vuetify-v2.2.11-brown.svg)](https://shields.io/)
[![Turf](https://img.shields.io/badge/turf-v3.0.14-yellow.svg)](https://shields.io/)
[![Vuex](https://img.shields.io/badge/vuex-v3.4.0-purple.svg)](https://shields.io/)


# Cliente

## Instalaçao das dependencias
```
npm install
```

### Compilando e subindo versão de desenvolvimento
```
npm run serve
```

### Gerando build para produção
```
npm run build
```

### Ajustando espaçamentos e outros detalhes
```
npm run lint
```