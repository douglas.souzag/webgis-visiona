import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '.././store';
import Home from '../views/Home/Home.vue'
import Areas from '../views/Areas/Areas.vue'
import Galeria from '../views/Galeria/Galeria.vue'
import Login from '../views/Login/Login.vue'
import Signin from '../views/Signin/Signin.vue'
import Usuarios from '../views/Usuarios/Usuarios.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '*',
    redirect: '/login',
  },
  {
    path: '/webgis',
    name: 'Home',
    component: Home,
    meta: {
      autenticado: true
    }
  },
  {
    path: '/areas',
    name: 'Areas',
    component: Areas,
    meta: {
      autenticado: true
    }
  },
  {
    path: '/galeria',
    name: 'Galeria',
    component: Galeria,
    meta: {
      autenticado: true
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      autenticado: false
    }
  },
  {
    path: '/signin',
    name: 'Signin',
    component: Signin,
    meta: {
      autenticado: false
    }
  },
  {
    path: '/users',
    name: 'Users',
    component: Usuarios,
    meta: {
      autenticado: true,
      admin: true
    }
  }
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes
})

router.beforeEach((to, from, next) => {
  store.dispatch('fetchAccessToken');
  if (to.fullPath === '/login'){
    if (store.state.accessToken) {
      next('/webgis');
    }
  }
  if (to.meta.admin === true) {
    if (!store.state.admin) {
      next('/login');
    } else {
      next();
    }
  }
  if (to.meta.autenticado === true) {
    if (!store.state.accessToken) {
      next('/login');
    } else {
      next();
    }
  }

  next();
});

export default router
