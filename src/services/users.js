import { http } from './config'
import store from '.././store';

export default{
    autenticar:(login) => {
        return http.post('authenticate',login)
    },
    criarUsuario:(account) => {
        return http.post('users',account)
    },
    listarUsuarios:() => {
        const headers = {
            'x_access_token': store.state.accessToken
        }
        
        return http.get('users',{headers})
    },
    deletarUsuario:(id) => {
        const headers = {
            'x_access_token': store.state.accessToken
        }

        const data = {
            id:id
        }
        console.log(headers)
        console.log(data)
        
        return http.delete('users',{headers,data})
    }
}
