import { http } from './config'
import store from '.././store';

export default{
    retornarImagens:(id_area,pagina) => {
        const headers = {
            'x_access_token': store.state.accessToken
        }
        const params = {
            'area_id': id_area,
            'page': pagina
        }
        return http.get('images',{params,headers})
    },
    retornarImagensProcessadas:(id_area,pagina) => {
        const headers = {
            'x_access_token': store.state.accessToken
        }
        const params = {
            'area_id': id_area,
            'page': pagina
        }
        return http.get('images-processed',{params,headers})
    },
    retornarAreas:(pagina) => {

        const headers = {
            'x_access_token': store.state.accessToken
        }
        const params = {
            page: pagina
        }
        return http.get('areas',{headers,params})
    },
    adicionarArea:(objetoArea) => {
        const headers = {
            'x_access_token': store.state.accessToken
        }
        return http.post('areas',objetoArea,{headers})
    },
    deletarArea:(id_area) => {
        const headers = {
            'x_access_token': store.state.accessToken
        }
        return http.delete('areas/'+id_area.toString(),{headers})
    },
    baixarImagens:(id_area) => {

        const data = {id: id_area}
        const headers = {
            'x_access_token': store.state.accessToken
        }
        return http.post('download',data,{headers})
    },
    processarImagem:(id_area,id_imagem) => {

        const data = {id: id_imagem,area_id: id_area}
        const headers = {
            'x_access_token': store.state.accessToken
        }
        return http.post('process',data,{headers})
    },
}