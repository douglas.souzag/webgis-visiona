import Users from '../../services/users'


export default {
    data() {
        return {
            arrayUsuarios: null
        }
    },
    methods: {
        deletarUsuario(id){
            Users.deletarUsuario(id).then(resposta =>{
            var removeIndex = this.arrayUsuarios.map(function(id) { return id; }).indexOf(id);
            this.arrayUsuarios.splice(removeIndex, 1);
                console.log(resposta)
            })
        }
    },
    mounted () {
        Users.listarUsuarios().then(resposta =>{
          this.arrayUsuarios = resposta.data;
          console.log(this.arrayUsuarios)
        })
    }
}