//Services
import Areas from '../../services/areas'

export default {
    components: {
    },
    data () {
      return {
        arrayAreas: [],
        link : '',
        pagina: 1,
        total_paginas: null,
        modalAreas: false,
        arrayImages: null,
        dialog: false,
      }
    },
    mounted () {
      Areas.retornarAreas(this.pagina).then(resposta =>{
        this.arrayAreas = resposta.data.areas;
        this.total_paginas = resposta.data.pages
      })
    },
    methods: {
      atualizarLista(){
        Areas.retornarAreas(this.pagina).then(resposta =>{
          this.arrayAreas = resposta.data.areas;
        })
      },
      abrirGaleria(area){
        localStorage.setItem('area', JSON.stringify(area))
        this.$router.push('/galeria')
      },
      deletarArea(area_id){
        Areas.deletarArea(area_id).then(resposta =>{
          console.log(resposta)
          var removeIndex = this.arrayAreas.map(function(item) { return item.id; }).indexOf(area_id);
          this.arrayAreas.splice(removeIndex, 1);
        })
      }
    },
  }
