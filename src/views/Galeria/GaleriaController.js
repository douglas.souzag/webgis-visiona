import Areas from '../../services/areas'

export default {
    data () {
        return {
            arrayImages: null,
            arrayImagesProcessadas: null,
            area : null,
            pagina: 1,
            pagina_p: 1,
            total_paginas: null,
            total_paginas_p: null,
        }
    },
    methods: {
        baixarImagens(area_id){
            Areas.baixarImagens(area_id).then(resposta =>{
              console.log(resposta)
            })
        },
        processarImagem(area_id,imagem_id){
            Areas.processarImagem(area_id,imagem_id).then(resposta =>{
              console.log(resposta)
            })
        },
        atualizarLista(){
            Areas.retornarImagens(this.area.id,this.pagina).then(resposta =>{
              this.arrayImages = resposta.data.images;
            })
        },
        atualizarListaProcessadas(){
            Areas.retornarImagensProcessadas(this.area.id,this.pagina_p).then(resposta =>{
              this.arrayImagesProcessadas = resposta.data.images;
            })
        }
    },
    mounted () {
        this.area = JSON.parse(localStorage.getItem('area'))
        Areas.retornarImagens(this.area.id,1).then(resposta =>{
            console.log(resposta.data.images.length)
            if (resposta.data.images.length === 0 || resposta.data.images.length === undefined){
                this.arrayImages = null
            } else {
                this.arrayImages = resposta.data.images   
                this.total_paginas = resposta.data.pages  
                console.log(this.arrayImages)
            }
        })
        Areas.retornarImagensProcessadas(this.area.id,1).then(resposta =>{
            console.log(resposta.data.images.length)
            if (resposta.data.images.length === 0 || resposta.data.images.length === undefined){
                this.arrayImagesProcessadas = null
            } else {
                this.arrayImagesProcessadas = resposta.data.images   
                this.total_paginas_p = resposta.data.pages  
                console.log(this.arrayImagesProcessadas)
            }
        })
        
      },
}