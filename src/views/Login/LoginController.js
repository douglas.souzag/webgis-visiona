import User from '../../services/users'
import router from "../../router";

export default{
  data() {
    return {
      login: {
        email: null,
        password: null
      },
      loading: false,
      errors : []
    }
  },
  methods: {
    onClickOutside () {
        this.active = false
      },
    auth(){
      this.loading = true
      User.autenticar(this.login).then(resposta => {

        localStorage.setItem('accessToken', resposta.data.token)
        localStorage.setItem('username', resposta.data.username)
        localStorage.setItem('email', resposta.data.email)
        
        this.loading = false
        
        if (resposta.data.token){
          this.$router.push('/webgis')
        }
      })
      .catch(err => {
        localStorage.removeItem('accessToken')
        localStorage.removeItem('username')
        localStorage.removeItem('email')
        this.errors.push(`Não foi possível efetuar seu login, motivo: ${err}`)
        this.loading = false
      })
    },
    create(){
      router.push('/signin')
    }
  }
}
