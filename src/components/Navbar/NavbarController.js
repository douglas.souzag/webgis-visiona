export default {
  data(){
    return {
      arrayRoutes : [
        {
          'nome': 'WebGIS',
          'route': 'webgis'
        },
        {
          'nome': 'Areas',
          'route': 'areas'
        }
      ],
      email: null,
      username: null,
      admin: null,

    }
  },
  methods: {
    logout(){
        localStorage.removeItem('accessToken')
        localStorage.removeItem('username')
        localStorage.removeItem('email')

        this.$router.push('/login')
    }
  },
  mounted() {
    this.email = localStorage.getItem('email')
    this.username = localStorage.getItem('username')
    this.admin = localStorage.getItem('admin')

    var adm = {
      'nome': 'Painel do Admin',
      'route': 'users'
    }

    console.log(this.admin)

    if (this.admin == 'true'){
      this.arrayRoutes.push(adm)
    }



  },
}